﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CalcHoras
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        int p = 0;//posição em digitos
        int l = 0;

        int h = 0;//horas já convertido
        int m = 0;//minutos já convertido

        string v1,v2;

        int v_p1;//dividir a string em duas partes 1/2
        int v_p2;//dividir a string em duas partes 2/2

        int op = 0; //escolha da operacao escolhida clicado op=1->SOMA op=2->SUBTRAÇÃO op=3->MULTIPLICAÇÃO op=4->DIVISÃO
        int res;//resposta total da operação.

        private void gridDigitos_Click(object sender, RoutedEventArgs e)
        {
            if(e.OriginalSource is Button)
            {
                Button bclick = (Button)e.OriginalSource;
                if(l>0)
                {
                    limpar();
                    l = 0;
                    jtvisor.Text += bclick.Tag.ToString();
                    if (p > 0)
                    {
                        jtvisor.Text = jtvisor.Text.Replace(":", "");
                        p = jtvisor.Text.Length - 2;
                        jtvisor.Text = jtvisor.Text.Insert(p, ":");
                    }


                    if (jtvisor.Text.Length == 3)
                    {
                        p = 0;
                        p = jtvisor.Text.Length - 2;
                        jtvisor.Text = jtvisor.Text.Insert(p, ":");
                    }

                }else
                {
                    jtvisor.Text += bclick.Tag.ToString();
                    if (p > 0)
                    {
                        jtvisor.Text = jtvisor.Text.Replace(":", "");
                        p = jtvisor.Text.Length - 2;
                        jtvisor.Text = jtvisor.Text.Insert(p, ":");
                    }

                    if (jtvisor.Text.Length == 3)
                    {
                        p = 0;
                        p = jtvisor.Text.Length - 2;
                        jtvisor.Text = jtvisor.Text.Insert(p, ":");
                    }
                }
            }
        }

        private void gridOperadores_Click(object sender, RoutedEventArgs e)
        {
            
            Button boper = (Button)e.OriginalSource;
            
            if (boper.Tag.ToString() == "C")
            {
                limpar();
            }

            if(boper.Tag.ToString()=="+")
            {
                v1 = jtvisor.Text;
                op = 1;
                verificarHoraValida(boper);
                jthitorico.Text += "> "+jtvisor.Text + boper.Tag.ToString();
                l++;
            }

            if (boper.Tag.ToString() == "-")
            {
                v1 = jtvisor.Text;
                op = 2;
                verificarHoraValida(boper);

                l++;
            }

            if (boper.Tag.ToString() == "*")
            {
                v1 = jtvisor.Text;
                op = 3;
                verificarHoraValida(boper);
                l++;
            }

            if (boper.Tag.ToString() == "/")
            {
                v1 = jtvisor.Text;
                op = 4;
                verificarHoraValida(boper);
                l++;
            }

            if (boper.Tag.ToString() == "=")
            {
                v2 = jtvisor.Text;
                verificarHoraValida(boper);
                l++;
            }
        }


        public void limpar()
        {
            v1 = jtvisor.Text;
            jtvisor.Text = "";
                            
            p = 0;
            h = 0;
            m = 0;
            
        }

        public void verificarHoraValida(Button bope)
        {
            if(bope.Tag.ToString()=="+" || bope.Tag.ToString() == "-" || bope.Tag.ToString() == "*" || bope.Tag.ToString() == "/")
            {
                if (v1.Length > 2)
                {
                }
                else
                {
                    if (v1.Length == 1)
                    {
                        jtvisor.Text = "0" + v1 + ":00";
                    }
                    if (v1.Length == 2)
                    {
                        jtvisor.Text = v1 + ":00";
                    }
                }
            }
            else
            {
                if(bope.Tag.ToString() == "=")
                {
                    if(op!=3)
                    {
                        if (v2.Length > 2)
                        {

                        }
                        else
                        {

                            if (v2.Length == 1)
                            {
                                jtvisor.Text = "0" + v2 + ":00";
                            }
                            if (v2.Length == 2)
                            {
                                jtvisor.Text = v2 + ":00";
                            }
                        }
                    }
                    jthitorico.Text += jtvisor.Text;
                    operacao();
                }
            }
        }
        
        private void operacao()
        {
            v2 = jtvisor.Text;
            dividirHoras(v1);
            TimeSpan min1 = new TimeSpan(v_p1, v_p2, 0);
            int soma1 = ((min1.Days * 24) + min1.Hours) * 60 + (min1.Minutes);
            v_p1 = 0;
            v_p2 = 0;
            if(op!=3)
            {
                dividirHoras(v2);
                TimeSpan min2 = new TimeSpan(v_p1, v_p2, 0);
                int soma2 = ((min2.Days * 24) + min2.Hours) * 60 + (min2.Minutes);
                if (op == 1)
                {
                    res = soma1 + soma2;
                }
                if (op == 2)
                {
                    res = soma1 - soma2;
                }
            }else
            {
                int cv2=Convert.ToInt32(v2);
                
                if (op == 3)
                {
                    res = soma1 * cv2;
                }

                if (op == 4)
                {
                    res = soma1 / cv2;
                }
            }
            //TimeSpan t1 = TimeSpan.ParseExact(v1, "c", CultureInfo.InvariantCulture);
            //TimeSpan t2 = TimeSpan.ParseExact(v2, "c", CultureInfo.InvariantCulture);
            imprimirResultado(res);
        }

        public void dividirHoras(string valor)
        {
            
            Char delimiter = ':';
            String[] substrings = valor.Split(delimiter);
            int cont = 0;
            foreach (var substring in substrings)
            {
                if(cont==0)
                {
                    v_p1 = Convert.ToInt32(substring);
                    cont++;
                }else
                {
                    v_p2 = Convert.ToInt32(substring); 
                }
               
            }
                
        }
        
        public void imprimirResultado(int res)
        {
            int min = Convert.ToInt32(res);
            while (min != 0)
            {
                if (min >= 60)
                {
                    min = min - 60;
                    if (min >= 0)
                    {
                        h++;
                    }
                    else
                    {
                        m = min;
                    }
                }
                else
                {
                    m = min;
                    min = 0;
                }
            }
            
            string ch = Convert.ToString(h);
            string cm = Convert.ToString(m);
            if (ch.Length == 1)
            {
                ch = "0" + ch;
            }
            
            if (cm.Length == 1)
            {
                cm = ":0"+cm;
            }

            if (cm.Length == 2)
            {
                cm = ":" + cm;
            }


            jtvisor.Text = ch + cm;
            jthitorico.Text += "="+jtvisor.Text+"\n";
        }

    }
}
